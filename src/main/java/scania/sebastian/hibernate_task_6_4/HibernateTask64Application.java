package scania.sebastian.hibernate_task_6_4;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(
        info = @Info(
                title = "My API",
                description = "My awesome API containing Skills, Students & Teachers"))

public class HibernateTask64Application {

    public static void main(String[] args) {
        SpringApplication.run(HibernateTask64Application.class, args);
    }

}
