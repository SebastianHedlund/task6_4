package scania.sebastian.hibernate_task_6_4.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Entity
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @OneToMany
    @JoinColumn(name = "teacher_id")
    List<Student> students;

    @JsonGetter("students")
    public List<String> students() {
        if(students != null) {
            return students.stream()
                    .map(student -> {
                        return "/api/v1/students/" + student.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name = "skill_id")}
    )
    public List<Skill> skills;

    @JsonGetter("skills")
    public List<String> skillsGetter() {
        if(skills != null){
            return skills.stream()
                    .map(skill -> {
                        return "/api/v1/skills/" + skill.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

}
