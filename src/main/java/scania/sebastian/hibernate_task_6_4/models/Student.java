package scania.sebastian.hibernate_task_6_4.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;

    @ManyToOne
    @JoinColumn(name = "teacher_id")
    public Teacher teacher;

    @JsonGetter("teacher")
    public String teacher() {
        if(teacher != null){
            return "/api/v1/teachers/" + teacher.getId();
        }else{
            return null;
        }
    }
}
