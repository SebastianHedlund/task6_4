package scania.sebastian.hibernate_task_6_4.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Getter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Entity
public class Skill {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    @ManyToMany
    @JoinTable(
            name = "teacher_skill",
            joinColumns = {@JoinColumn(name = "skill_id")},
            inverseJoinColumns = {@JoinColumn(name = "teacher_id")}
    )
    public List<Teacher> teachers;

    @JsonGetter("teachers")
    public List<String> teachers() {
        return teachers.stream()
                .map(teacher -> {
                    return "/api/v1/books/" + teacher.getId();
                }).collect(Collectors.toList());
    }

}
