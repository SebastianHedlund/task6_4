package scania.sebastian.hibernate_task_6_4.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import scania.sebastian.hibernate_task_6_4.models.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
}
