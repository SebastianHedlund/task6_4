package scania.sebastian.hibernate_task_6_4.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import scania.sebastian.hibernate_task_6_4.models.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
