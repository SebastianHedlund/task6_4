package scania.sebastian.hibernate_task_6_4.dataaccess;

import org.springframework.data.jpa.repository.JpaRepository;
import scania.sebastian.hibernate_task_6_4.models.Skill;

public interface SkillRepository extends JpaRepository<Skill, Long> {
}
