package scania.sebastian.hibernate_task_6_4.controllers;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.hibernate_task_6_4.dataaccess.SkillRepository;
import scania.sebastian.hibernate_task_6_4.models.Skill;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/skills")
@Tag(name = "Skills", description = "Different skills a Teacher might have. ")
public class SkillController {
    @Autowired
    private SkillRepository skillRepository;

    @Operation(summary = "Get all available skills")
    @GetMapping
    public ResponseEntity<List<Skill>> getAllSkills(){
        return new ResponseEntity<>(skillRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Get a specific skill correlated to the provided ID.")
    @GetMapping("/{id}")
    public ResponseEntity<Skill> getSkill(@PathVariable Long id){
        if(!skillRepository.existsById(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(skillRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Post a new skill to the database.")
    @PostMapping
    public ResponseEntity<Skill> addSkill(@RequestBody Skill skill){
        return new ResponseEntity<>(skillRepository.save(skill), HttpStatus.CREATED);
    }

    @Operation(summary = "Replace a skill in the database.")
    @PutMapping("/{id}")
    public ResponseEntity<Skill> updateSkill(@RequestBody Skill skill, @PathVariable Long id){
        if(!Objects.equals(id, skill.getId())){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(skillRepository.save(skill), HttpStatus.NO_CONTENT);
    }

}
