package scania.sebastian.hibernate_task_6_4.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.hibernate_task_6_4.dataaccess.TeacherRepository;
import scania.sebastian.hibernate_task_6_4.models.Teacher;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/teachers")
@Tag(name = "Teachers", description = "Teachers, connected to students and skills.")
public class TeacherController {
    @Autowired
    private TeacherRepository teacherRepository;

    @Operation(summary = "Get all available teachers")
    @GetMapping
    public ResponseEntity<List<Teacher>> getAllTeachers(){
        return new ResponseEntity<>(teacherRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Get a specific teacher correlated to the provided ID.")
    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getTeacher(@PathVariable Long id){
        if(!teacherRepository.existsById(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(teacherRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Post a new teacher to the database.")
    @PostMapping
    public ResponseEntity<Teacher> addTeacher(@RequestBody Teacher teacher){
        return new ResponseEntity<>(teacherRepository.save(teacher), HttpStatus.CREATED);
    }

    @Operation(summary = "Replace a teacher in the database.")
    @PutMapping("/{id}")
    public ResponseEntity<Teacher> updateTeacher(@RequestBody Teacher teacher, @PathVariable Long id){
        if(!Objects.equals(id, teacher.getId())){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(teacherRepository.save(teacher), HttpStatus.NO_CONTENT);
    }
}
