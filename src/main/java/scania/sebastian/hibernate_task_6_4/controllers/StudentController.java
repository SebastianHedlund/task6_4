package scania.sebastian.hibernate_task_6_4.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import scania.sebastian.hibernate_task_6_4.dataaccess.StudentRepository;
import scania.sebastian.hibernate_task_6_4.models.Student;

import java.util.List;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/students")
@Tag(name = "Students", description = "Students, connected to Teachers")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @Operation(summary = "Get all available students")
    @GetMapping
    public ResponseEntity<List<Student>> getAllStudents(){
        return new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
    }

    @Operation(summary = "Get a specific student correlated to the provided ID.")
    @GetMapping("/{id}")
    public ResponseEntity<Student> getStudent(@PathVariable Long id){
        if(!studentRepository.existsById(id))
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        return new ResponseEntity<>(studentRepository.findById(id).get(), HttpStatus.OK);
    }

    @Operation(summary = "Post a new student to the database.")
    @PostMapping
    public ResponseEntity<Student> addStudent(@RequestBody Student student){
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.CREATED);
    }

    @Operation(summary = "Replace a student in the database.")
    @PutMapping("/{id}")
    public ResponseEntity<Student> updateStudent(@RequestBody Student student, @PathVariable Long id){
        if(!Objects.equals(id, student.getId())){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(studentRepository.save(student), HttpStatus.NO_CONTENT);
    }
}
